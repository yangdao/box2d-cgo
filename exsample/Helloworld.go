package main

import (
	"fmt"

	box2d "git.oschina.net/yangdao/box2d-cgo"
)

func main() {
	switch 2 {
	case 1:
		RunHelloWorld()
	case 2:
		RunTest1()
	case 3:
		RunSomeShape()
	}
}

func CreateBall(world *box2d.B2World, PosX, PosY float32) *box2d.B2Body {
	bodyDef := box2d.NewB2BodyDef()
	bodyDef.SetType(box2d.B2_dynamicBody)
	bodyDef.Position().Set(PosX, PosY)
	bodyDef.SetFixedRotation(true)

	body := world.CreateBody(bodyDef)
	dynamicBox := box2d.NewB2PolygonShape()
	dynamicBox.SetAsBox(0.5, 0.5)
	fixtureDef := box2d.NewB2FixtureDef()
	fixtureDef.SetShape(uintptr(dynamicBox))
	fixtureDef.SetDensity(1.0)
	fixtureDef.SetFriction(0.3)
	fixtureDef.SetUserData(box2d.GenUserData("卡鲁宾asssss"))
	body.CreateFixtureByDef(fixtureDef)
	return body
}
func StepNumber(world *box2d.B2World, number int, fn func()) {
	var timeStep float32 = 1.0 / 60.0
	velocityIterations := int32(6)
	positionIterations := int32(2)
	for i := 0; i < number; i++ {
		world.Step(timeStep, velocityIterations, positionIterations)
		if fn != nil {
			fn()
		}
	}
}

func RunHelloWorld() {
	fmt.Println("Hello World")
	gray := box2d.NewB2Vec2(0.0, -10.0)

	world := box2d.NewB2World(gray)

	groundBodyDef := box2d.NewB2BodyDef()

	groundBodyDef.Position().Set(0.0, -10.0)

	groundBody := world.CreateBody(groundBodyDef)

	groundBox := box2d.NewB2PolygonShape()
	groundBox.SetAsBox(50.0, 10.0)

	groundBody.CreateFixture(uintptr(groundBox), 0.0)

	//dynamic body
	body := CreateBall(world, 0.0, 4.0)

	StepNumber(world, 60, func() {
		fmt.Println(body.GetPosition().GetX(), body.GetPosition().GetY())
	})
}
