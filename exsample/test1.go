package main

import (
	"fmt"

	box2d "git.oschina.net/yangdao/box2d-cgo"
	//"unsafe"
)

func RunTest1() {
	gray := &box2d.B2Vec2{X: 0, Y: -10}
	world := box2d.NewB2World(gray)
	wallBody := world.CreateBody(box2d.NewB2BodyDef())
	body := CreateBall(world, 0, 10)
	listener := box2d.NewB2ContactListener()
	world.SetContactListener(listener.Ptr)

	listener.AddBeginContactEvent(func(contact box2d.B2Contact) {
		fmt.Println(box2d.GetUserData(contact.GetFixtureA().GetUserData()))
		fmt.Println(box2d.GetUserData(contact.GetFixtureB().GetUserData()))
		/*
					 void* fixtureUserData = contact->GetFixtureA()->GetUserData();
			        if ( (int)fixtureUserData == 3 )
			            numFootContacts++;
			        //check if fixture B was the foot sensor
			        fixtureUserData = contact->GetFixtureB()->GetUserData();
			        if ( (int)fixtureUserData == 3 )
			            numFootContacts++;

		*/

	})
	listener.AddEndContactEvent(func(contact box2d.B2Contact) {
		fmt.Println("are you ok>")
	})

	edgeShape := box2d.NewB2EdgeShape()
	edgeShape.Set(box2d.NewB2Vec2(-10, 0), box2d.NewB2Vec2(10, 0))
	wallBody.CreateFixture(uintptr(edgeShape), 0.0)

	StepNumber(world, 150, func() {
		fmt.Println(body.GetPosition().GetX(), body.GetPosition().GetY())
	})
}
