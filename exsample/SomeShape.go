package main

import (
	"fmt"

	box2d "git.oschina.net/yangdao/box2d-cgo"
)

func RunSomeShape() {
	world := box2d.NewB2World(box2d.NewB2Vec2(0, -10))

	bd := box2d.NewB2BodyDef()
	bd.SetFixedRotation(true)
	bd.Position().Set(0, 3)
	bd.SetType(box2d.B2_dynamicBody)
	playerBody := world.CreateBody(bd)

	shapeVec := box2d.NewCVec2Array(
		box2d.B2Vec2{-0.4, 0.5}, box2d.B2Vec2{0.4, 0.5},
		box2d.B2Vec2{0.5, 0.4}, box2d.B2Vec2{0.5, -0.4},
		box2d.B2Vec2{0.4, -0.5}, box2d.B2Vec2{-0.4, -0.5},
		box2d.B2Vec2{-0.5, -0.4}, box2d.B2Vec2{-0.5, 0.4})
	polyShape := box2d.NewB2PolygonShape()
	polyShape.Set(shapeVec, 8)
	fdef := box2d.NewB2FixtureDef()
	fdef.SetDensity(20.0)
	fdef.SetShape(uintptr(polyShape))
	playerBody.CreateFixtureByDef(fdef)

	StepNumber(world, 60, func() {
		fmt.Println(playerBody.GetPosition().GetX(), playerBody.GetPosition().GetY())
	})
}
