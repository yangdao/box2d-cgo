package box2d_cgo

/*
#cgo LDFLAGS: -L . -lgobox2d -lBox2D
#cgo CFLAGS: -I ./
#include "box2d.h"
*/
import "C"
import (
	"unsafe"
)

type B2Contact uintptr

func (this B2Contact) GetManifold() B2Manifold {
	return B2Manifold(C.Contact_GetManifold(unsafe.Pointer(this)))
}

func (this B2Contact) GetFixtureA() B2Fixture {
	return B2Fixture(C.Contact_GetFixtureA(unsafe.Pointer(this)))
}
func (this B2Contact) GetFixtureB() B2Fixture {
	return B2Fixture(C.Contact_GetFixtureB(unsafe.Pointer(this)))
}
func (this B2Contact) GetIsTouching() bool {
	bnum := int(C.Contact_GetIsTouching(unsafe.Pointer(this)))
	if bnum == 0 {
		return false
	} else {
		return true
	}
}

type B2ContactEdge uintptr

func (this B2ContactEdge) GetContact() B2Contact {
	return B2Contact(C.ContactEdge_GetContact(unsafe.Pointer(this)))
}
func (this B2ContactEdge) GetNext() B2ContactEdge {
	return B2ContactEdge(C.ContactEdge_GetNext(unsafe.Pointer(this)))
}

func (this B2ContactEdge) GetPrev() B2ContactEdge {
	return B2ContactEdge(C.ContactEdge_GetPrev(unsafe.Pointer(this)))
}
