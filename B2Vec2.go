package box2d_cgo

/*
#cgo LDFLAGS: -L . -lgobox2d -lBox2D
#cgo CFLAGS: -I ./
#include "box2d.h"
*/
import "C"
import (
	"unsafe"
)

type CB2Vec2 uintptr

func (this *B2Vec2) ToCGOVec2() uintptr {
	ptr := C.NewVec2(C.float(this.X), C.float(this.Y))
	return uintptr(ptr)
}

func (this CB2Vec2) Set(x float32, y float32) {
	C.Vec2_Set(unsafe.Pointer(this), C.float(x), C.float(y))
}

func (this CB2Vec2) GetX() float32 {
	return float32(C.Vec2_GetX(unsafe.Pointer(this)))
}

func (this CB2Vec2) GetY() float32 {
	return float32(C.Vec2_GetY(unsafe.Pointer(this)))
}

type CVec2Array uintptr

func NewCVec2Array(vecs ...B2Vec2) CVec2Array {
	var XArrPos *[10]float32 = &[10]float32{}
	var YArrPos *[10]float32 = &[10]float32{}
	for i := 0; i < len(vecs); i++ {
		XArrPos[i] = vecs[i].X
		YArrPos[i] = vecs[i].Y
	}
	return CVec2Array(C.CNewCVec2Array(C.int(len(vecs)), (*C.float)(&XArrPos[0]), (*C.float)(&YArrPos[0])))
}
