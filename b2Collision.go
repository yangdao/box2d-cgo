package box2d_cgo

/*
#cgo LDFLAGS: -L . -lgobox2d -lBox2D
#cgo CFLAGS: -I ./
#include "box2d.h"
*/
import "C"
import (
	"unsafe"
)

type B2Manifold uintptr

func (this B2Manifold) GetLocalNormal() CB2Vec2 {
	return CB2Vec2(C.Manifold_GetLocalNormal(unsafe.Pointer(this)))
}
