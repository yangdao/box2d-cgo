package box2d_cgo

/*
#cgo LDFLAGS: -L . -lgobox2d -lBox2D
#cgo CFLAGS: -I ./
#include "box2d.h"
*/
import "C"
import (
	"unsafe"
)

type B2PolygonShape uintptr

func NewB2PolygonShape() B2PolygonShape {
	return B2PolygonShape(C.NewPolygonShape())
}

func (this B2PolygonShape) SetAsBox(x float32, y float32) {
	C.PolygonShape_SetAsBox(unsafe.Pointer(this), C.float(x), C.float(y))
}
func (this B2PolygonShape) SetAsBox4(x float32, y float32, pos *B2Vec2, angle float32) {
	C.PolygonShape_SetAsBox4(unsafe.Pointer(this), C.float(x), C.float(y), unsafe.Pointer(pos.ToCGOVec2()), C.float(angle))
}
func (this B2PolygonShape) Set(arr CVec2Array, size int) {
	C.PolygonShape_Set(unsafe.Pointer(this), unsafe.Pointer(arr), C.int(size))
}

type B2EdgeShape uintptr

func NewB2EdgeShape() B2EdgeShape {
	return B2EdgeShape(C.NewEdgeShape())
}

func (this B2EdgeShape) Set(x, y *B2Vec2) {
	C.EdgeShape_Set(unsafe.Pointer(this), unsafe.Pointer(x.ToCGOVec2()), unsafe.Pointer(y.ToCGOVec2()))
}

type B2CircleShape uintptr

func NewB2CircleShape() B2CircleShape {
	return B2CircleShape(C.NewCircleShape())
}

func (this B2CircleShape) SetRadius(radius float32) {
	C.CircleShape_SetRadius(unsafe.Pointer(this), C.float(radius))
}

func NewNoCornerPolygonVec(w, h float32, wsub, hsub float32, offsetX, offsetY float32) CVec2Array {
	halfw := w * 0.5
	halfh := h * 0.5
	shapeVec := NewCVec2Array(
		B2Vec2{ /*-0.4*/ (-halfw + wsub) + offsetX, halfh + offsetY /*0.5*/}, B2Vec2{ /*0.4*/ (halfw - wsub) + offsetX, halfh + offsetY /*0.5*/},
		B2Vec2{ /*0.5*/ halfw + offsetX, (halfh - hsub) + offsetY /*0.4*/}, B2Vec2{halfw + offsetX /*0.5*/, (-halfh + hsub) + offsetY /* -0.4*/},
		B2Vec2{ /*0.4*/ (halfw - wsub) + offsetX, -halfh + offsetY /*-0.5*/}, B2Vec2{(-halfw + wsub) + offsetX /*-0.4*/, -halfh + offsetY /*-0.5*/},
		B2Vec2{ /*-0.5*/ -halfw + offsetX, (-halfh + hsub) + offsetY /*-0.4*/}, B2Vec2{ /*-0.5*/ -halfw + offsetX, (halfh - hsub) + offsetY /*0.4*/})

	return shapeVec
}
