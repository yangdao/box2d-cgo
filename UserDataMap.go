package box2d_cgo

import (
	"unsafe"
)

var userData map[int]interface{}
var CurSymId int = 1

func GenUserData(data interface{}) unsafe.Pointer {
	if userData == nil {
		userData = make(map[int]interface{})
	}
	userData[CurSymId] = data
	CurSymId++
	return unsafe.Pointer(uintptr(CurSymId - 1))
}

func GetUserData(ptr unsafe.Pointer) interface{} {
	intKey := (int)(uintptr(ptr))
	return userData[intKey]
}

func DestoryUserData(userDataKey unsafe.Pointer) {
	delete(userData, (int)(uintptr(userDataKey)))
}
