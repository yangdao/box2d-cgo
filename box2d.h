#ifdef __cplusplus
#include <Box2D/Box2D.h>
#include <stdio.h>
extern "C" {
#endif

#include <stdio.h>
extern void go_BeginContact(void* ptr_GO,void* contact);
extern void go_EndContact(void* ptr_GO,void* contact);

void* NewVec2(float x,float y);
void Vec2_Set(void* ptr,float x,float y);

float Vec2_GetX(void* ptr_vec2);
float Vec2_GetY(void* ptr_vec2);
void* CNewCVec2Array(int size,float *XPosArr, float *YPosArr);
///////////////////////////////////////World/////////////////////////////////////////////
void* NewWorld(float x,float y);
void* World_CreateBody(void* ptr_world,void* ptr_bodydef);
void World_Step(void* ptr_world,float dt,int velocityIterations,int positionIterations);
void World_SetContactListener(void* ptr_world,void* ptr_ContactListener);
///////////////////////////////////////Body//////////////////////////////////////////////
void* NewBodyDef();
void* BodyDef_Get_Position(void* ptr);
void BodyDef_Set_Type(void* ptr_bodydef,int typ);
void BodyDef_SetFixedRotation(void* ptr_bodydef,int b);

void* Body_CreateFixture(void* ptr_Body,void* ptr_shape,float density);
void* Body_CreateFixtureByDef(void* ptr_Body,void* ptr_fixtureDef);
void* Body_GetPosition(void* ptr_Body);
void  Body_SetTransform(void* ptr_Body,float posX,float posY,float angle);
float Body_GetAngle(void* ptr_Body);
void  Body_SetLinearVelocity(void* ptr_Body,void* ptr_vec);
void* Body_GetLinearVelocity(void* ptr_Body);
void  Body_ApplyLinearImpulse(void* ptr_Body,void* ptr_impulse,void* ptr_point,int awake);
void* Body_GetWorldCenter(void* ptr_Body);
void  Body_SetGravityScale(void* ptr_Body,float scale);
void  Body_SetLinearDamping(void* ptr_Body,float linearDamping);
void* Body_GetGetContactList(void* ptr_Body);
///////////////////////////////////////Shape/////////////////////////////////////////////
void* NewPolygonShape();
void  PolygonShape_SetAsBox(void* ptr_PolygonShape,float x,float y);
void  PolygonShape_SetAsBox4(void* ptr_PolygonShape,float x,float y,void* ptr_vec,float angle);
void  PolygonShape_Set(void* ptr_PolygonShape,void* ptr_vecArr,int size);

void* NewEdgeShape();
void  EdgeShape_Set(void* ptr_edgeShape,void* ptr_vec1,void* ptr_vec2);

void* NewCircleShape();
void  CircleShape_SetRadius(void* ptr_circleShape,float radius);
///////////////////////////////////////Fixture/////////////////////////////////////////////
void* NewFixtureDef();
void FixtureDef_Set_Shape(void* ptr_fixtureDef,void* ptr_shape);
void FixtureDef_Set_Density(void* ptr_fixtureDef,float density);
void FixtureDef_Set_Friction(void* ptr_fixtureDef,float friction);
void FixtureDef_Set_isSensor(void* ptr_fixtureDef,int isSensor);
void FixtureDef_Set_UserData(void* ptr_fixtureDef,void* userData);

void* Fixture_Get_UserData(void* ptr_fixture);
///////////////////////////////////////ContactListener/////////////////////////////////////
void* NewGoContactListener(void* ptr_Go);
void* Contact_GetManifold(void* ptr_Contact);
void* Manifold_GetLocalNormal(void* ptr_Manifold);
void* Contact_GetFixtureA(void* ptr_Contact);
void* Contact_GetFixtureB(void* ptr_Contact);
int Contact_GetIsTouching(void* ptr_Contact);
///////////////////////////////////////B2ContactEdge///////////////////////////////////////
void* ContactEdge_GetContact(void* ptr_ContactEdge);
void* ContactEdge_GetNext(void* ptr_ContactEdge);
void* ContactEdge_GetPrev(void* ptr_ContactEdge);
#ifdef __cplusplus
}
#endif
