package box2d_cgo

/*
#cgo LDFLAGS: -L . -lgobox2d -lBox2D
#cgo CFLAGS: -I ./
#include "box2d.h"
*/
import "C"
import "unsafe"

const (
	B2_staticBody    = 0
	B2_kinematicBody = 1
	B2_dynamicBody   = 2
)

type B2BodyDef struct {
	ptr unsafe.Pointer
}

func NewB2BodyDef() *B2BodyDef {
	return &B2BodyDef{
		ptr: C.NewBodyDef(),
	}
}
func (b *B2BodyDef) Position() CB2Vec2 {
	return CB2Vec2(C.BodyDef_Get_Position(b.ptr))
}
func (this *B2BodyDef) SetType(typ int) {
	C.BodyDef_Set_Type(this.ptr, C.int(typ))
}

func (this *B2BodyDef) SetFixedRotation(b bool) {
	if b {
		C.BodyDef_SetFixedRotation(this.ptr, 1)
	} else {
		C.BodyDef_SetFixedRotation(this.ptr, 0)
	}
}

type B2Body struct {
	ptr unsafe.Pointer
}

func (this *B2Body) CreateFixture(shape uintptr, density float32) B2Fixture {
	return B2Fixture(C.Body_CreateFixture(this.ptr, unsafe.Pointer(shape), C.float(density)))
}
func (this *B2Body) CreateFixtureByDef(def B2FixtureDef) B2Fixture {
	return B2Fixture(C.Body_CreateFixtureByDef(this.ptr, unsafe.Pointer(def)))
}

func (this *B2Body) GetPosition() CB2Vec2 {
	return CB2Vec2(C.Body_GetPosition(this.ptr))
}
func (this *B2Body) SetTransform(x, y, angle float32) {
	C.Body_SetTransform(this.ptr, C.float(x), C.float(y), C.float(angle))
}

func (this *B2Body) GetAngle() float32 {
	return float32(C.Body_GetAngle(this.ptr))
}

func (this *B2Body) SetLinearVelocity(vec *B2Vec2) {
	C.Body_SetLinearVelocity(this.ptr, unsafe.Pointer(vec.ToCGOVec2()))
}

func (this *B2Body) GetLinearVelocity() CB2Vec2 {
	return CB2Vec2(C.Body_GetLinearVelocity(this.ptr))
}

func (this *B2Body) GetContactList() B2ContactEdge {
	return B2ContactEdge(C.Body_GetGetContactList(this.ptr))
}

func (this *B2Body) ApplyLinearImpulse(impulse *B2Vec2, point uintptr, wake bool) {
	if wake {
		C.Body_ApplyLinearImpulse(this.ptr, unsafe.Pointer(impulse.ToCGOVec2()), unsafe.Pointer(point), 1)
	} else {
		C.Body_ApplyLinearImpulse(this.ptr, unsafe.Pointer(impulse.ToCGOVec2()), unsafe.Pointer(point), 0)
	}
}
func (this *B2Body) GetWorldCenter() CB2Vec2 {
	return CB2Vec2(C.Body_GetWorldCenter(this.ptr))
}

func (this *B2Body) SetGravityScale(scale float32) {
	C.Body_SetGravityScale(this.ptr, C.float(scale))
}

func (this *B2Body) SetLinearDamping(linerDamping float32) {
	C.Body_SetLinearDamping(this.ptr, C.float(linerDamping))
}
