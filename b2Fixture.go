package box2d_cgo

/*
#cgo LDFLAGS: -L . -lgobox2d -lBox2D
#cgo CFLAGS: -I ./
#include "box2d.h"
*/
import "C"
import (
	"unsafe"
)

type B2Fixture uintptr
type B2FixtureDef uintptr

func NewB2FixtureDef() B2FixtureDef {
	return B2FixtureDef(C.NewFixtureDef())
}

func (this B2FixtureDef) SetShape(shape uintptr) {
	C.FixtureDef_Set_Shape(unsafe.Pointer(this), unsafe.Pointer(shape))
}

func (this B2FixtureDef) SetDensity(density float32) {
	C.FixtureDef_Set_Density(unsafe.Pointer(this), C.float(density))
}

func (this B2FixtureDef) SetFriction(friction float32) {
	C.FixtureDef_Set_Friction(unsafe.Pointer(this), C.float(friction))
}

func (this B2FixtureDef) SetisSensor(isSensor bool) {
	if isSensor {
		C.FixtureDef_Set_isSensor(unsafe.Pointer(this), 1)
	} else {
		C.FixtureDef_Set_isSensor(unsafe.Pointer(this), 0)
	}
}

func (b B2FixtureDef) SetUserData(ptr unsafe.Pointer) {
	C.FixtureDef_Set_UserData(unsafe.Pointer(b), ptr)
}

func (this B2Fixture) GetUserData() unsafe.Pointer {
	return C.Fixture_Get_UserData(unsafe.Pointer(this))
}
