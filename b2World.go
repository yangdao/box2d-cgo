package box2d_cgo

/*
#cgo LDFLAGS: -L . -lgobox2d -lBox2D
#cgo CFLAGS: -I ./
#include "box2d.h"
*/
import "C"
import (
	"unsafe"
)

type B2World struct {
	ptr unsafe.Pointer
}

func NewB2World(gravity *B2Vec2) *B2World {
	retWorld := &B2World{}
	retWorld.ptr = C.NewWorld(C.float(gravity.X), C.float(gravity.Y))
	return retWorld
}

func (this *B2World) CreateBody(bodydef *B2BodyDef) *B2Body {
	bodyPtr := C.World_CreateBody(this.ptr, bodydef.ptr)
	return &B2Body{ptr: bodyPtr}
}

func (this *B2World) Step(dt float32, velocityIterations int32, positionIterations int32) {
	C.World_Step(this.ptr, C.float(dt), C.int(velocityIterations), C.int(positionIterations))
}

func (this *B2World) SetContactListener(ptrListener unsafe.Pointer) {
	C.World_SetContactListener(this.ptr, ptrListener)
}
