#include <Box2D/Box2D.h>
#include <stdio.h>
#include "box2d.h"
class GoContactListener : public b2ContactListener
{
	public:
		void* Go_Ptr;
		GoContactListener(void* ptr_Go)
		{
			Go_Ptr = ptr_Go;
		}

        virtual void BeginContact(b2Contact* contact)
        {
          go_BeginContact(Go_Ptr,contact);
        }

        virtual void EndContact(b2Contact* contact)
        {
          go_EndContact(Go_Ptr,contact);
        }
};